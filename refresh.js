const googleAuth = require('google-auth-library')

// for prod
// ENV
function refreshToken(rfrshToken) {
    const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URL } = process.ENV
    const gAuth = new googleAuth()
    const oauth2Client = new gAuth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL)
    return new Promise((resolve, reject) => {
        oauth2Client.refreshToken(rfrshToken, (err, tokens, response) => {
            if (err) reject(err)
            resolve(tokens)
        })
    })
}

module.exports = refreshToken