const googleAuth = require('google-auth-library')

// for test
function testRefreshToken(clientId, clientSecret, redirectUrl, rfrshToken) {
    const gAuth = new googleAuth()
    const oauth2Client = new gAuth.OAuth2(clientId, clientSecret, redirectUrl)
    return new Promise((resolve, reject) => {
        oauth2Client.refreshToken(rfrshToken, (err, tokens, response) => {
            if (err) reject(err)
            console.log(tokens)
            resolve(response)
        })
    })
}

// simple test case
(async function test() {
    try {
        const CLIENT_SECRET = require('./client_secret.json')

        const clientId = CLIENT_SECRET.installed.client_id
        const clientSecret = CLIENT_SECRET.installed.client_secret
        const redirectUrl = CLIENT_SECRET.installed.redirect_uris[0]

        const TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
            process.env.USERPROFILE) + '/.credentials/'
        const TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json'

        const rfrshToken = require(TOKEN_PATH).refresh_token

        const reps = await testRefreshToken(clientId, clientSecret, redirectUrl, rfrshToken)
        console.log(reps)
    } catch (e) {
        console.error(e)
    }
})()